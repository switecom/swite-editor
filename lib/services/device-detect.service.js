(function() {
angular.module('switeEditor')
.service('deviceDetect', deviceDetect);

function deviceDetect() {
    return {
        isMobile: function() {
            var result = (navigator.userAgent.match('Android') || navigator.userAgent.match('BlackBerry') || navigator.userAgent.match('iPhone|iPad|iPod') || navigator.userAgent.match('Opera Mini') || navigator.userAgent.match('IEMobile'));
            return (result !== null);
        },
        isIpad: function () {
            return navigator.userAgent.match('iPad');
        },
        isIOS: function isIOS() {
            return navigator.userAgent.match('iPhone|iPad|iPod');
        }
    };
}
})();
