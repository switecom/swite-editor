(function() {
angular.module('switeEditor')
.factory('EditorUi', editorUiFactory);

function editorUiFactory($compile, $window, Editable) {
    var uiTemplate;
    class EditorUi extends EventEmitter {
        static setUiTemplate(template) {
            console.debug("EditorUi: setTemplate");
            if (!template) {
                throw new Error("Missing UI Template");
            }
            uiTemplate = template;
        }

        constructor(data) {
            if (!(data instanceof Editable)) {
                throw new Error("Need Editable instance");
            }
            super();
            this.data = data;
        }

        getScope() {
            return this.data.getScope();
        };

        deleteScope() {
            if (this.data) { delete this.data; }
        };

        // Compila il template della UI e gli assegna lo scope dello widget che sta venendo editato
        compileUi () {
            console.debug("EditorUi: compile");
            var template;
            if (!uiTemplate) {
                throw new Error("Can't compile UI. Missing data.");

            }
            template = uiTemplate.clone();
            var linkFn = $compile(template);
            this.uiElement = linkFn(this.data.getScope());
        }

        renderUi() {
            console.debug("EditorUi: render");
            this.compileUi();

            if (!this.uiElement) { throw new Error("Missing UI Element"); }

            //TODO: trying to prepend to UI to the editable element this.page = angular.element('#page');
            this.page = angular.element(this.data.getElement()[0]);
            this.page.prepend(this.uiElement);

            this.uiToolbarElement = this.uiElement.find('#toolbar');

            this.setUiPosition();
        }

        setUiPosition() {
            var editableElement = this.data.getElement();
            var rect = editableElement[0].getBoundingClientRect();
            var offsetLeft = this.page[0].getBoundingClientRect().left;
            var offsetTop = angular.element('.content').scrollTop();

            this.uiElement.removeAttr('style');
            this.uiElement.css('right', -10);
            this.uiElement.css('top',  -9);

            // Each button is 48px, plus 3px border for each side
            this.uiToolbarElement.css('min-width', 48 * this.data.getButtonsAmount() + 6);

            this.uiElement.css('z-index', 3);
            editableElement.css('z-index', 2);
            editableElement.addClass('edit-mode');
            editableElement.css('position', editableElement.css('position') === 'static' ? 'relative' : editableElement.css('position'));
        }

        removeUi() {
            console.debug("EditorUi: removeUI");

            var query = `[ui-editable-id="${this.data.getId()}"]`;
            var UI = angular.element(query);
            if (!UI || UI.length === 0) { return; }

            UI.find("#toolbar").remove();
            UI.remove();

            this.data.getElement().removeClass('edit-mode');

            // blockFactory.removeListener('blockMoved', this.setUiPosition);

            if (this.uiSidebarSync) {
                this.uiSidebarSync();
            }

            this.data.getElement().removeAttr('style');
            // this.deleteScope();
        }

        uiCss(property, value) {
            return this.uiElement.css(property, value);
        }
    }
    return EditorUi;
}
})();
