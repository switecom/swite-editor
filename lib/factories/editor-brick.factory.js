(function() {
angular.module('switeEditor')
.factory('EditorBrick', editorBrickFactory);

function editorBrickFactory(EditorUi) {
    class EditorBrick extends EditorUi {
        constructor(data) {
            console.debug("EditorBrick: constructor");
            if (!data) { throw new Error("Missing data"); }
            super(data);
        }

        setUiPosition() {
            var editableElement = this.data.getElement();
            var rect = editableElement[0].getBoundingClientRect();
            var offsetLeft = this.page[0].getBoundingClientRect().left;
            var offsetTop = angular.element('.content').scrollTop();

            this.uiElement.removeAttr('style');
            this.uiElement.css('position', 'absolute');
            this.uiElement.css('left', 0);
            this.uiElement.css('top', 0);
            this.uiElement.css('min-height', rect.height);
            this.uiElement.css('width', rect.width);
        }

        render() {
            console.debug("EditorBrick: render");
            this.renderUi();
            if (!this.data.isCustomizable()) {
                return;
            }
        }

        remove() {
            console.debug("EditorBrick: remove");
            this.removeUi();
            // TODO WTF
            // if (this.data.getOptions().pl && this.options.placeholderText) { delete this.options.placeholderText; }
        }

    }
    return EditorBrick;
}

})();
