(function() {
angular.module('switeEditor')
.factory('switeEditor', switeEditorFactory);

function switeEditorFactory($templateRequest, _, EventEmitter, EditorUi, EditorTextArea, EditorArray, EditorBrick, EditorGeneric, Editable) {
    var editor, editable;
    var templates = {
        ui: {}
    };
    var editorOptions;
    var _isOpen = false;
    var emitter = new EventEmitter();

    emitter.init = init;
    emitter.isOpen = isOpen;
    emitter.render = render;
    emitter.refresh = refresh;
    emitter.remove = remove;
    emitter.getScope = getScope;
    emitter.uiCss = uiCss;
    emitter.setOptions = setOptions;
    emitter.selectText = selectText;
    emitter.selectLastChar = selectLastChar;
    emitter.loadUiTemplate = loadUiTemplate;
    emitter.getEditables = getEditables;

    return emitter;

    function init(options) {
        var tasks = [];

        if (!templates.text) {
            tasks.push($templateRequest(options.text.templatePath));
        }

        if (!templates.array) {
            tasks.push($templateRequest(options.array.templatePath));
        }

        editorOptions = options || {};

        return Promise.all(tasks)
        .then((res) => {
            templates.text = angular.element(res[0]);
            templates.array = angular.element(res[1]);
        });
    }

    function isOpen() {
        return _isOpen;
    }

    function setOptions(options) {
        editorOptions = options || {};
    }

    function loadUiTemplate(path, type) {
        return $templateRequest(path)
        .then((uiTemplate) => {
            templates.ui[type] = uiTemplate;
        });
    }

    function onTaModelChange(newVal) {
        emitter.emit('TAModel.change', newVal);
    }

    function render(data) {
        if (isOpen()) {
            remove();
        }


        EditorUi.setUiTemplate(angular.element(templates.ui[data.uiTemplateType]));
        data = _.omit(data, ['uiTemplateType']);
        var extended = _.assign(data, {
            template: angular.element(templates[data.editable.type]),
            options: editorOptions[data.editable.type]
        });

        editable = new Editable(extended);
        var pluginName = editable.getPlugin();
        var editorClass = eval(pluginName);

        emitter.emit(`opened`);

        editor = new editorClass(editable);
        editor.on('TAModel.change', onTaModelChange);
        editor.render();
        _isOpen = true;
    }

    function remove() {
        if (!isOpen()) {
            return;
        }

        editor.removeListener('TAModel.change', onTaModelChange);
        editor.remove();
        _isOpen = false;

        emitter.emit(`closed`);
        emitter.emit(`closed.${editable.getId()}`);
    }

    function getScope() {
        return editable.getScope();
    }

    function getEditables() {
        return editor.getEditables();
    }

    function uiCss(property, value) {
        return editor.uiCss(property, value);
    }

    function refresh(data) {
        if (editor instanceof EditorArray) {
            return editor.refresh(data);
        }

        throw new Error(`Using editorArray.refresh method on ${editor.constructor.name} instance`);
    }

    function selectText() {
        if (editor instanceof editorTextArea) {
            return editor.selectText();
        }
        throw new Error(`Using editorTextArea.selectText method on ${editor.constructor.name} instance`);
    }

    function selectLastChar() {
        if (editor instanceof editorTextArea) {
            return editor.selectLastChar();
        }
        throw new Error(`Using editorTextArea.selectLastChar method on ${editor.constructor.name} instance`);
    }
}

})();
