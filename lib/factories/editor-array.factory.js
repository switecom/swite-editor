(function() {
angular.module('switeEditor')
.factory('EditorArray', editorArrayFactory);

function editorArrayFactory($rootScope, $compile, deviceDetect, EditorUi, $timeout) {

    /*  la configurazione per froala dei campi che compongono le varie voci dell'array 
        toolbar custom per evitare che froala disegni la sua con bottoni e frecce
        Questo perchè la toolbar vuota di froala contiene sempre la freccia absolute che indica il campo modificabile
    */
    const froalaFieldConfig = {
        toolbarInline: true,
        charCounterCount: false,
        toolbarContainer: '#toolbar_'
    };

    class EditorArray extends EditorUi {
        constructor(data) {
            console.debug("EditorArray: constructor");
            if (!data) { throw new Error("Missing data"); }
            super(data);
        }

        getEditables(){
            let arrayItems = {};
            _.forEach(this.scope.editables, (value, key)=> {    // per ciascun elemento dell'array
                let isValidItem = false,
                    arrayItem = {},
                    itemKeys = Object.keys(value);
                _.forEach(itemKeys, (itemKey)=>{
                    if (value[itemKey].text && value[itemKey].text.length) {
                        arrayItem[itemKey] = value[itemKey];
                        isValidItem = true;
                    }
                });
                if (isValidItem){
                    arrayItems[key] = arrayItem;
                }
            });
            return arrayItems;
        }

        // Compila il template della TextArea e gli assegna lo scope dello widget che sta venendo editato
        compile() {
            console.debug("EditorArray: compile");
            this.scope = this.data.getScope().$new(true);
            let options = this.data.getOptions();
            this.trashElement = `<span ng-click="removeItem()" title="${options.messages.removeElement}" class="sw-widget-array-trash">
                                    <i class="fa fa-trash"></i>
                                </span>`;
            this.addElement = `<li class="li" id="add-item" ng-click="addItem()" title="${options.messages.addElement}">
                                    <span class="cursor-pointer" translate>Add an element</span>
                                    <span><i class="fa fa-plus"></i></span>
                                </li>`;
            this.scope.compileFields = compileFields.bind(this);
            this.scope.removeAllItems = removeAllItems.bind(this);
            this.scope.removeItem = removeItem.bind(this);
            this.scope.addItem = addItem.bind(this);
        }

        render() {
            console.debug('EditorArray: render');
            this.renderUi();
            //When toolbar froala open, means that open virtual Keyboard mobile
            angular.element('body').addClass('sw-isFroalaActive');
            if (!this.data.isCustomizable()) {  // se non è customizzabile non si fa nulla
                return Promise.resolve();
            }
            this.compile();
            // TODO: FIX THIS SHIT
            if ($rootScope.editingEditableArray === undefined){
                $rootScope.editingEditableArray = {};
            }
            this.originalScope = this.data.getScope().$$childHead; // || this.data.getScope(); // this.originalScope = this.data.scope; // this.originalScope = scope;
            return this.mapEditable();
        }


        mapEditable(){
            return new Promise(( resolve )=>{
                var scope = this.scope,
                    editable = {},
                    edData = this.data.getEditableData(),
                    tpl;

                scope.editables = {};
                $rootScope.editingEditableArray[this.data.editable.object.id] = true;

                for (let i = 0; i < edData.length; i++ ){
                    scope.editables['' + i] = {};
                    for (let field in edData[i]){
                        scope.editables['' + i][field] = {
                            text: edData[i][field] + "" // son costretto a convertire a stringa
                        };
                    }
                }

                if (!edData.length){
                    this.generateItem(0)
                }

                tpl = angular.element(this.data.arrayTemplate).find('[sw-editable-list-template]')
                tpl.removeAttr('sw-editable-list-template').attr('sw-editable-list', '').attr('id', "tpl_copy");
                let fn = $compile(angular.element(tpl));
                fn(scope, (tpl_comp)=>{
                    this.data.getElement().find('[sw-editable-list]').parent().append(tpl_comp);
                    $timeout();
                    // senza timeout non parte la digest e non disegna nulla
                    $timeout(()=>{
                        editable.list = tpl_comp;
                        editable.listItems = editable.list.find('li').not('#add-item');
                        var self = this;
                        // For each list item
                        editable.listItems.each(function (itemIndex) {
                            // per ciascun elemento ne compilo i field
                            let itemElement = angular.element(this); 
                            self.processEditable(itemElement, itemIndex);
                        });
                        var plus = angular.element(this.addElement);
                        editable.list.append(plus);
                        $compile(plus)(scope);
                        resolve();
                    });
                });
            });
        }
        /**
         * 
         * @param {*} itemElement 
         * @param {*} itemIndex 
         */
        processEditable(itemElement, itemIndex){
            let indexItem_str = "" + itemIndex,
                scope = this.scope,
                fields = itemElement.find('[sw-editable-field]');
            itemElement.attr('item-index', indexItem_str);
            // Creates a dedicated scope object for the editable
            scope.compileFields(fields, itemIndex);
            // Set removal icon
            this.addTrash(itemElement, indexItem_str);            
        }
        /**
         * 
         */
        remove() {
            console.debug('EditorArray: remove');
            if (!this.data.isCustomizable()) {
                return this.removeUi();
            }
            this.removeUi();
            //When toolbar froala closed, means that virtual Keyboard mobile closed
            angular.element('body').removeClass('sw-isFroalaActive');
            this.data.getElement().find('#tpl_copy').remove();
            // TODO fix tha shit
            $rootScope.editingEditableArray[this.data.editable.object.id] = false;
        }
        /**
         * @param {*} data 
         * @description ricarica tutto l'array widget con nuovi dati
         */
        refresh(data) {
            console.debug('EditorArray: refresh', data.length);
            var newData = data; 
            return this.scope.removeAllItems()
                .then(()=>{
                    if (newData) {
                        this.data.editable.object.data = newData;
                    } else {
                        data = this.data.getEditableData();
                    }
                    this.data.getElement().find('#tpl_copy').remove();
                    return this.mapEditable();
                });
        }
        /**
         * @param {*} element 
         * @param {*} elementId 
         * @param {*} scope
         * @description appende il cestino 
         */
        addTrash( element, elementId ){
            let trash = angular.element(this.trashElement);
            trash.attr('ng-click', 'removeItem("' + elementId + '")');
            element.append(trash);
            $compile(trash)(this.scope);
        }

        generateItem(newIdx, data){
            this.scope.editables["" + newIdx] = {};
            _.forEach(this.data.editable.object.placeholders, (value, key) => {
                this.scope.editables["" + newIdx][key] = {
                    text: (data && data[key]) ? data[key] : ''
                };
            });
        }

    }
    return EditorArray;


    /**
     * 
     * @param {*} fields 
     * @param {*} itemIndex 
     */
    function compileFields(fields, itemIndex) {
        var editableId = "" + itemIndex,
            editorArray = this,
            scope = this.scope,
            options = this.data.getOptions(),
            that = this;
        // Add class to all fields
        fields.addClass('editable-field');
        // For each field of the editable
        fields.each(function (fieldIndex) {
            // List Item field element
            let fieldElement = angular.element(this),
                fieldIdentifier,
                ngModelId,
                froalaConfig;
            // Gets the identifier of the field assigned to the attribute (title, date_from, etc)
            fieldIdentifier = fieldElement.attr('sw-editable-field');
            if (!fieldIdentifier) { return; }
            // Generate the key for ngModel to bind the scope to the field element
            ngModelId = `editables["${editableId}"].${fieldIdentifier}.text`;

            fieldElement
                .data({ 'fieldIdentifier': fieldIdentifier, 'editableId': editableId })
                .attr('ng-model', ngModelId);

            
            // inizializzo froala per il campo
            froalaConfig = froalaFieldConfig;
            froalaConfig.placeholderText = editorArray.data.getPlaceholderFieldText(fieldIdentifier);            
            
            // compondo l'id della toolbar custom ('#toolbar_' + editableId + '_' + fieldIdentifier)
            froalaConfig.toolbarContainer += editableId + '_' + fieldIdentifier;
            // genero l'elemento toolbar come div vuoto e non visibile 
            let hiddenToolbar = angular.element("<div id = '" + froalaConfig.toolbarContainer + "'></div>").css('display', 'none');
            // appendo la toolbar custom all'item dell'array
            fieldElement.closest('li').append(hiddenToolbar);
            
            $compile(fieldElement)(scope);

            fieldElement.froalaEditor(froalaConfig);
            fieldElement.on('froalaEditor.keyup', function (e, editor, keyupEvent) {
                let frElem = angular.element(this),
                    editableFieldValue = "", //frElem.text()
                    editableFieldValueHtml = frElem.froalaEditor('html.get');
                if (editableFieldValueHtml.length !== 0) {
                    editableFieldValueHtml = angular.element(editableFieldValueHtml);
                    editableFieldValue = editableFieldValueHtml.text();
                }
                if (scope.editables[frElem.data('editableId')][frElem.data('fieldIdentifier')] === undefined) {
                    scope.editables[frElem.data('editableId')][frElem.data('fieldIdentifier')] = {};
                }
                scope.editables[frElem.data('editableId')][frElem.data('fieldIdentifier')].text = editableFieldValue;
            });
            
        });
        return fields;
    }
    /**
     * @description rimuove dallo scope l'oggetto editable con indice = index
     * @param {String} index 
     */
    function removeItem(index) {
        delete this.scope.editables[index];
    }
    /**
     * @description rimuove dallo scope tutti gli oggetti editable
     */
    function removeAllItems() {
        return new Promise( (resolve) => {
            let itemsKey = Object.keys( this.scope.editables);
            _.forEach(itemsKey, ( k )=>{
                this.scope.removeItem( k );
            });
            $timeout(()=>{ resolve(); });
        });
    }

    function addItem(data) {
        return new Promise(( resolve ) => {
            let newIdx = this.data.getElement().find('[sw-editable-list] li').not('#add-item').length;
            this.generateItem(newIdx, data);
            $timeout(() => {
                let itemElement = angular.element(this.data.getElement().find('[sw-editable-list] li')[newIdx]);
                this.processEditable(itemElement, newIdx);
                resolve();
            });
        });
    }

}

})();
