(function() {
angular.module('switeEditor')
.factory('EditorGeneric', editorGenericFactory);

function editorGenericFactory(EditorUi) {
    class EditorGeneric extends EditorUi {
        constructor(data) {
            console.debug("EditorGeneric: constructor");
            if (!data) { throw new Error("Missing data"); }
            super(data);
        }

        render() {
            console.debug("EditorGeneric: render");
            this.renderUi();
        }

        remove() {
            console.debug("EditorGeneric: remove");
            this.removeUi();
            // TODO WTF
            // if (this.data.getOptions().pl && this.options.placeholderText) { delete this.options.placeholderText; }
        }

    }
    return EditorGeneric;
}

})();
