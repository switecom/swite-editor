(function() {
angular.module('switeEditor')
.factory('Editable', editableFactory);

function editableFactory() {
    class Editable {
        static checkData(data) {
            const properties = [
                'template',
                'options',
                'editable.element',
                'editable.type',
                'editable.object',
                'editable.object.id',
                'editable.object.data',
                'editable.object.isPlaceholder',
                'editable.object.isCustomizable',
                'scope',
                'buttonsAmount'
            ];
            return _.every(properties, (property) => {
                return _.has(data, property);
            });
        }

        constructor(data) {
            if (!Editable.checkData(data)) {
                throw new Error('Missing data');
            }
            _.assign(this, data);
           this.editable.element = angular.element(this.editable.element);
        }

        getPlaceholderFieldText(fieldIdentifier) {
            return this.editable.object.placeholders[fieldIdentifier];
        }

        getPlaceholderText() {
            return this.editable.object.placeholderText;
        }

        getOptions() {
            return this.options;
        }

        getElement() {
            return this.editable.element;
        }

        getId() {
            return this.editable.object.id;
        }

        getEditableData() {
            return this.editable.object.data;
        }

        getScope() {
            // console.log(" EDITABLE -> getScope", this.scope)
            return this.scope;
        }
        getEditables() {
            console.error("EDITBLE getEditables")
            return this.scope.editables;
        }

        getButtonsAmount() {
            return this.buttonsAmount;
        }

        isPlaceholder() {
            return this.editable.object.isPlaceholder;
        }

        isCustomizable() {
            return this.editable.object.isCustomizable;
        }

        getType() {
            return this.editable.type;
        }

        getPlugin() {
            return this.options.plugin;
        }

    }

    return Editable;
}

})();
