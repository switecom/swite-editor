(function() {
angular.module('switeEditor')
.factory('EditorTextArea', editorTextAreaFactory);

function editorTextAreaFactory($compile, deviceDetect, EditorUi) {
    class EditorTextArea extends EditorUi {
        constructor(data) {
            console.debug("EditorTextArea: constructor");
            if (!data) { throw new Error("Missing data"); }
            super(data);
        }

        selectText(DOMElement) {
            setTimeout(function () {
                var doc = document,
                    range, selection;
                if (doc.body.createTextRange) {
                    range = doc.body.createTextRange();
                    range.moveToElementText(element);
                    range.select();
                } else if (window.getSelection) {
                    selection = window.getSelection();
                    range = doc.createRange();
                    range.selectNodeContents(DOMElement);
                    selection.removeAllRanges();
                    selection.addRange(range);
                }
            });
        };

        selectLastChar(DOMElement) {
            DOMElement.focus();
            if (typeof window.getSelection != "undefined" && typeof document.createRange != "undefined") {
                var range = document.createRange();
                range.selectNodeContents(DOMElement);
                range.collapse(false);
                var sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);
            } else if (typeof document.body.createTextRange != "undefined") {
                var textRange = document.body.createTextRange();
                textRange.moveToElementText(DOMElement);
                textRange.collapse(false);
                textRange.select();
            }
        }

        // Compila il template della TextArea e gli assegna lo scope dello widget che sta venendo editato
        compile() {
            console.debug("EditorTextArea: compile");
            var scope = this.data.getScope();
            var editableElement = this.data.getElement();
            var options = this.data.getOptions();

            options.heightMin = editableElement.height();
            options.placeholderText = this.data.getPlaceholderText();

            //When toolbar froala open, means that open virtual Keyboard mobile
            angular.element('body').addClass('sw-isFroalaActive');

            scope.TAModel = this.data.isPlaceholder() ? "" : this.data.getEditableData();

            this.taChangeListener = scope.$watch("TAModel", (newVal, oldVal) => {
                this.emit('TAModel.change', newVal);
            });

            this.taOriginalEditable = editableElement.find('[sw-editable]');
            if (this.taOriginalEditable.attr('editable-options')) {
                let extendedOptions = this.taOriginalEditable.attr('editable-options');
                try {
                    this.data.options = options = _.assign(options, JSON.parse(extendedOptions));
                } catch(e) {
                    console.debug(`Error extending textarea options: ${e.message}`);
                }
            }

            scope.TAOptions = options;
            this.taEditableStyle = window.getComputedStyle(this.taOriginalEditable[0]);

            this.taEditable = this.taOriginalEditable.clone();
            this.taOriginalEditable.replaceWith(this.taEditable);

            this.taEditable.attr('froala', 'TAOptions');
            this.taEditable.attr('ng-model', 'TAModel');
            var linkFn = $compile(this.taEditable);
            this.taElement = linkFn(scope);
        }

        render() {
            this.renderUi();
            if (!this.data.isCustomizable()) {
                return;
            }
            this.compile();
            if (!this.taElement || !this.uiElement || !this.taEditable) { throw new Error("Missing data"); }

            this.taToolbar = angular.element('.fr-toolbar');

            // Prevent a scrollbar to show on widgets textarea in certain cases
            this.taEditable.find('.fr-wrapper').css('overflow', 'hidden');

            angular.element('[data-param1="background"]').remove();

            // Make sure that the text in froala editor has the same style of the original text
            var frElement = this.taEditable.find('.fr-element');
            var frPlaceholder = this.taEditable.find('.fr-placeholder');
            frElement.css('text-align', this.taEditableStyle["text-align"]);
            frElement.css('min-width', this.data.getElement().width());
            frElement.css('color', this.taEditableStyle["color"]);
            frPlaceholder.css('text-align', this.taEditableStyle["text-align"]);
            frPlaceholder.css('min-width', this.data.getElement().width());

            frElement.attr('spellcheck', 'false');
            frElement.attr('autocomplete', 'off');

            // Remove fucking froala banner
            var ad = this.taElement.find('[href="https://froala.com/wysiwyg-editor"]');
            ad.remove();

            setTimeout(() => {
                // Focus the element if it's not, to open the soft keyboard
                frElement.focus();
                if (!this.data.isPlaceholder()) {
                    if (deviceDetect.isMobile() && !deviceDetect.isIpad()) {
                        //TODO:something for mobilegit
                    } else {
                        this.selectText(frElement[0]);
                        // Force show froala toolbar
                        this.taElement.froalaEditor('toolbar.showInline', null, true);
                    }
                }

            });
        }

        remove() {
            console.debug("EditorTextArea: remove");
            this.removeUi();

            //When toolbar froala removed, means that virtual Keyboard mobile closed
            angular.element('body').removeClass('sw-isFroalaActive');

            if (this.taToolbar) {
                this.taToolbar.remove();
            }

            if (this.taEditable) {
                this.taElement.replaceWith(this.taOriginalEditable);
            }

            if (this.taChangeListener) {
                this.taChangeListener();
            }
            // if (this.data.getOptions().pl && this.options.placeholderText) { delete this.options.placeholderText; }
        }

    }
    return EditorTextArea;
}

})();
