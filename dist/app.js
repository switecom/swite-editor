'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function () {
    angular.module('switeEditor', []);
})();

(function () {
    angular.module('switeEditor').controller('UiCtrl', UiCtrl);

    function UiCtrl($scope, switeEditor, editable) {
        var uiCtrl = this;
        uiCtrl.id = editable.getId();
        uiCtrl.showSourceButton = true;
    }
})();

(function () {
    angular.module('switeEditor').factory('Editable', editableFactory);

    function editableFactory() {
        var Editable = function () {
            _createClass(Editable, null, [{
                key: 'checkData',
                value: function checkData(data) {
                    var properties = ['template', 'options', 'editable.element', 'editable.type', 'editable.object', 'editable.object.id', 'editable.object.data', 'editable.object.isPlaceholder', 'editable.object.isCustomizable', 'scope', 'buttonsAmount'];
                    return _.every(properties, function (property) {
                        return _.has(data, property);
                    });
                }
            }]);

            function Editable(data) {
                _classCallCheck(this, Editable);

                if (!Editable.checkData(data)) {
                    throw new Error('Missing data');
                }
                _.assign(this, data);
                this.editable.element = angular.element(this.editable.element);
            }

            _createClass(Editable, [{
                key: 'getPlaceholderFieldText',
                value: function getPlaceholderFieldText(fieldIdentifier) {
                    return this.editable.object.placeholders[fieldIdentifier];
                }
            }, {
                key: 'getPlaceholderText',
                value: function getPlaceholderText() {
                    return this.editable.object.placeholderText;
                }
            }, {
                key: 'getOptions',
                value: function getOptions() {
                    return this.options;
                }
            }, {
                key: 'getElement',
                value: function getElement() {
                    return this.editable.element;
                }
            }, {
                key: 'getId',
                value: function getId() {
                    return this.editable.object.id;
                }
            }, {
                key: 'getEditableData',
                value: function getEditableData() {
                    return this.editable.object.data;
                }
            }, {
                key: 'getScope',
                value: function getScope() {
                    // console.log(" EDITABLE -> getScope", this.scope)
                    return this.scope;
                }
            }, {
                key: 'getEditables',
                value: function getEditables() {
                    console.error("EDITBLE getEditables");
                    return this.scope.editables;
                }
            }, {
                key: 'getButtonsAmount',
                value: function getButtonsAmount() {
                    return this.buttonsAmount;
                }
            }, {
                key: 'isPlaceholder',
                value: function isPlaceholder() {
                    return this.editable.object.isPlaceholder;
                }
            }, {
                key: 'isCustomizable',
                value: function isCustomizable() {
                    return this.editable.object.isCustomizable;
                }
            }, {
                key: 'getType',
                value: function getType() {
                    return this.editable.type;
                }
            }, {
                key: 'getPlugin',
                value: function getPlugin() {
                    return this.options.plugin;
                }
            }]);

            return Editable;
        }();

        return Editable;
    }
})();

(function () {
    angular.module('switeEditor').factory('EditorArray', editorArrayFactory);

    function editorArrayFactory($rootScope, $compile, deviceDetect, EditorUi, $timeout) {

        /*  la configurazione per froala dei campi che compongono le varie voci dell'array 
            toolbar custom per evitare che froala disegni la sua con bottoni e frecce
            Questo perchè la toolbar vuota di froala contiene sempre la freccia absolute che indica il campo modificabile
        */
        var froalaFieldConfig = {
            toolbarInline: true,
            charCounterCount: false,
            toolbarContainer: '#toolbar_'
        };

        var EditorArray = function (_EditorUi) {
            _inherits(EditorArray, _EditorUi);

            function EditorArray(data) {
                _classCallCheck(this, EditorArray);

                console.debug("EditorArray: constructor");
                if (!data) {
                    throw new Error("Missing data");
                }
                return _possibleConstructorReturn(this, (EditorArray.__proto__ || Object.getPrototypeOf(EditorArray)).call(this, data));
            }

            _createClass(EditorArray, [{
                key: 'getEditables',
                value: function getEditables() {
                    var arrayItems = {};
                    _.forEach(this.scope.editables, function (value, key) {
                        // per ciascun elemento dell'array
                        var isValidItem = false,
                            arrayItem = {},
                            itemKeys = Object.keys(value);
                        _.forEach(itemKeys, function (itemKey) {
                            if (value[itemKey].text && value[itemKey].text.length) {
                                arrayItem[itemKey] = value[itemKey];
                                isValidItem = true;
                            }
                        });
                        if (isValidItem) {
                            arrayItems[key] = arrayItem;
                        }
                    });
                    return arrayItems;
                }

                // Compila il template della TextArea e gli assegna lo scope dello widget che sta venendo editato

            }, {
                key: 'compile',
                value: function compile() {
                    console.debug("EditorArray: compile");
                    this.scope = this.data.getScope().$new(true);
                    var options = this.data.getOptions();
                    this.trashElement = '<span ng-click="removeItem()" title="' + options.messages.removeElement + '" class="sw-widget-array-trash">\n                                    <i class="fa fa-trash"></i>\n                                </span>';
                    this.addElement = '<li class="li" id="add-item" ng-click="addItem()" title="' + options.messages.addElement + '">\n                                    <span class="cursor-pointer" translate>Add an element</span>\n                                    <span><i class="fa fa-plus"></i></span>\n                                </li>';
                    this.scope.compileFields = compileFields.bind(this);
                    this.scope.removeAllItems = removeAllItems.bind(this);
                    this.scope.removeItem = removeItem.bind(this);
                    this.scope.addItem = addItem.bind(this);
                }
            }, {
                key: 'render',
                value: function render() {
                    console.debug('EditorArray: render');
                    this.renderUi();
                    //When toolbar froala open, means that open virtual Keyboard mobile
                    angular.element('body').addClass('sw-isFroalaActive');
                    if (!this.data.isCustomizable()) {
                        // se non è customizzabile non si fa nulla
                        return Promise.resolve();
                    }
                    this.compile();
                    // TODO: FIX THIS SHIT
                    if ($rootScope.editingEditableArray === undefined) {
                        $rootScope.editingEditableArray = {};
                    }
                    this.originalScope = this.data.getScope().$$childHead; // || this.data.getScope(); // this.originalScope = this.data.scope; // this.originalScope = scope;
                    return this.mapEditable();
                }
            }, {
                key: 'mapEditable',
                value: function mapEditable() {
                    var _this2 = this;

                    return new Promise(function (resolve) {
                        var scope = _this2.scope,
                            editable = {},
                            edData = _this2.data.getEditableData(),
                            tpl;

                        scope.editables = {};
                        $rootScope.editingEditableArray[_this2.data.editable.object.id] = true;

                        for (var i = 0; i < edData.length; i++) {
                            scope.editables['' + i] = {};
                            for (var field in edData[i]) {
                                scope.editables['' + i][field] = {
                                    text: edData[i][field] + "" // son costretto a convertire a stringa
                                };
                            }
                        }

                        if (!edData.length) {
                            _this2.generateItem(0);
                        }

                        tpl = angular.element(_this2.data.arrayTemplate).find('[sw-editable-list-template]');
                        tpl.removeAttr('sw-editable-list-template').attr('sw-editable-list', '').attr('id', "tpl_copy");
                        var fn = $compile(angular.element(tpl));
                        fn(scope, function (tpl_comp) {
                            _this2.data.getElement().find('[sw-editable-list]').parent().append(tpl_comp);
                            $timeout();
                            // senza timeout non parte la digest e non disegna nulla
                            $timeout(function () {
                                editable.list = tpl_comp;
                                editable.listItems = editable.list.find('li').not('#add-item');
                                var self = _this2;
                                // For each list item
                                editable.listItems.each(function (itemIndex) {
                                    // per ciascun elemento ne compilo i field
                                    var itemElement = angular.element(this);
                                    self.processEditable(itemElement, itemIndex);
                                });
                                var plus = angular.element(_this2.addElement);
                                editable.list.append(plus);
                                $compile(plus)(scope);
                                resolve();
                            });
                        });
                    });
                }
                /**
                 * 
                 * @param {*} itemElement 
                 * @param {*} itemIndex 
                 */

            }, {
                key: 'processEditable',
                value: function processEditable(itemElement, itemIndex) {
                    var indexItem_str = "" + itemIndex,
                        scope = this.scope,
                        fields = itemElement.find('[sw-editable-field]');
                    itemElement.attr('item-index', indexItem_str);
                    // Creates a dedicated scope object for the editable
                    scope.compileFields(fields, itemIndex);
                    // Set removal icon
                    this.addTrash(itemElement, indexItem_str);
                }
                /**
                 * 
                 */

            }, {
                key: 'remove',
                value: function remove() {
                    console.debug('EditorArray: remove');
                    if (!this.data.isCustomizable()) {
                        return this.removeUi();
                    }
                    this.removeUi();
                    //When toolbar froala closed, means that virtual Keyboard mobile closed
                    angular.element('body').removeClass('sw-isFroalaActive');
                    this.data.getElement().find('#tpl_copy').remove();
                    // TODO fix tha shit
                    $rootScope.editingEditableArray[this.data.editable.object.id] = false;
                }
                /**
                 * @param {*} data 
                 * @description ricarica tutto l'array widget con nuovi dati
                 */

            }, {
                key: 'refresh',
                value: function refresh(data) {
                    var _this3 = this;

                    console.debug('EditorArray: refresh', data.length);
                    var newData = data;
                    return this.scope.removeAllItems().then(function () {
                        if (newData) {
                            _this3.data.editable.object.data = newData;
                        } else {
                            data = _this3.data.getEditableData();
                        }
                        _this3.data.getElement().find('#tpl_copy').remove();
                        return _this3.mapEditable();
                    });
                }
                /**
                 * @param {*} element 
                 * @param {*} elementId 
                 * @param {*} scope
                 * @description appende il cestino 
                 */

            }, {
                key: 'addTrash',
                value: function addTrash(element, elementId) {
                    var trash = angular.element(this.trashElement);
                    trash.attr('ng-click', 'removeItem("' + elementId + '")');
                    element.append(trash);
                    $compile(trash)(this.scope);
                }
            }, {
                key: 'generateItem',
                value: function generateItem(newIdx, data) {
                    var _this4 = this;

                    this.scope.editables["" + newIdx] = {};
                    _.forEach(this.data.editable.object.placeholders, function (value, key) {
                        _this4.scope.editables["" + newIdx][key] = {
                            text: data && data[key] ? data[key] : ''
                        };
                    });
                }
            }]);

            return EditorArray;
        }(EditorUi);

        return EditorArray;

        /**
         * 
         * @param {*} fields 
         * @param {*} itemIndex 
         */
        function compileFields(fields, itemIndex) {
            var editableId = "" + itemIndex,
                editorArray = this,
                scope = this.scope,
                options = this.data.getOptions(),
                that = this;
            // Add class to all fields
            fields.addClass('editable-field');
            // For each field of the editable
            fields.each(function (fieldIndex) {
                // List Item field element
                var fieldElement = angular.element(this),
                    fieldIdentifier = void 0,
                    ngModelId = void 0,
                    froalaConfig = void 0;
                // Gets the identifier of the field assigned to the attribute (title, date_from, etc)
                fieldIdentifier = fieldElement.attr('sw-editable-field');
                if (!fieldIdentifier) {
                    return;
                }
                // Generate the key for ngModel to bind the scope to the field element
                ngModelId = 'editables["' + editableId + '"].' + fieldIdentifier + '.text';

                fieldElement.data({ 'fieldIdentifier': fieldIdentifier, 'editableId': editableId }).attr('ng-model', ngModelId);

                // inizializzo froala per il campo
                froalaConfig = froalaFieldConfig;
                froalaConfig.placeholderText = editorArray.data.getPlaceholderFieldText(fieldIdentifier);

                // compondo l'id della toolbar custom ('#toolbar_' + editableId + '_' + fieldIdentifier)
                froalaConfig.toolbarContainer += editableId + '_' + fieldIdentifier;
                // genero l'elemento toolbar come div vuoto e non visibile 
                var hiddenToolbar = angular.element("<div id = '" + froalaConfig.toolbarContainer + "'></div>").css('display', 'none');
                // appendo la toolbar custom all'item dell'array
                fieldElement.closest('li').append(hiddenToolbar);

                $compile(fieldElement)(scope);

                fieldElement.froalaEditor(froalaConfig);
                fieldElement.on('froalaEditor.keyup', function (e, editor, keyupEvent) {
                    var frElem = angular.element(this),
                        editableFieldValue = "",
                        //frElem.text()
                    editableFieldValueHtml = frElem.froalaEditor('html.get');
                    if (editableFieldValueHtml.length !== 0) {
                        editableFieldValueHtml = angular.element(editableFieldValueHtml);
                        editableFieldValue = editableFieldValueHtml.text();
                    }
                    if (scope.editables[frElem.data('editableId')][frElem.data('fieldIdentifier')] === undefined) {
                        scope.editables[frElem.data('editableId')][frElem.data('fieldIdentifier')] = {};
                    }
                    scope.editables[frElem.data('editableId')][frElem.data('fieldIdentifier')].text = editableFieldValue;
                });
            });
            return fields;
        }
        /**
         * @description rimuove dallo scope l'oggetto editable con indice = index
         * @param {String} index 
         */
        function removeItem(index) {
            delete this.scope.editables[index];
        }
        /**
         * @description rimuove dallo scope tutti gli oggetti editable
         */
        function removeAllItems() {
            var _this5 = this;

            return new Promise(function (resolve) {
                var itemsKey = Object.keys(_this5.scope.editables);
                _.forEach(itemsKey, function (k) {
                    _this5.scope.removeItem(k);
                });
                $timeout(function () {
                    resolve();
                });
            });
        }

        function addItem(data) {
            var _this6 = this;

            return new Promise(function (resolve) {
                var newIdx = _this6.data.getElement().find('[sw-editable-list] li').not('#add-item').length;
                _this6.generateItem(newIdx, data);
                $timeout(function () {
                    var itemElement = angular.element(_this6.data.getElement().find('[sw-editable-list] li')[newIdx]);
                    _this6.processEditable(itemElement, newIdx);
                    resolve();
                });
            });
        }
    }
})();

(function () {
    angular.module('switeEditor').factory('EditorBrick', editorBrickFactory);

    function editorBrickFactory(EditorUi) {
        var EditorBrick = function (_EditorUi2) {
            _inherits(EditorBrick, _EditorUi2);

            function EditorBrick(data) {
                _classCallCheck(this, EditorBrick);

                console.debug("EditorBrick: constructor");
                if (!data) {
                    throw new Error("Missing data");
                }
                return _possibleConstructorReturn(this, (EditorBrick.__proto__ || Object.getPrototypeOf(EditorBrick)).call(this, data));
            }

            _createClass(EditorBrick, [{
                key: 'setUiPosition',
                value: function setUiPosition() {
                    var editableElement = this.data.getElement();
                    var rect = editableElement[0].getBoundingClientRect();
                    var offsetLeft = this.page[0].getBoundingClientRect().left;
                    var offsetTop = angular.element('.content').scrollTop();

                    this.uiElement.removeAttr('style');
                    this.uiElement.css('position', 'absolute');
                    this.uiElement.css('left', 0);
                    this.uiElement.css('top', 0);
                    this.uiElement.css('min-height', rect.height);
                    this.uiElement.css('width', rect.width);
                }
            }, {
                key: 'render',
                value: function render() {
                    console.debug("EditorBrick: render");
                    this.renderUi();
                    if (!this.data.isCustomizable()) {
                        return;
                    }
                }
            }, {
                key: 'remove',
                value: function remove() {
                    console.debug("EditorBrick: remove");
                    this.removeUi();
                    // TODO WTF
                    // if (this.data.getOptions().pl && this.options.placeholderText) { delete this.options.placeholderText; }
                }
            }]);

            return EditorBrick;
        }(EditorUi);

        return EditorBrick;
    }
})();

(function () {
    angular.module('switeEditor').factory('EditorGeneric', editorGenericFactory);

    function editorGenericFactory(EditorUi) {
        var EditorGeneric = function (_EditorUi3) {
            _inherits(EditorGeneric, _EditorUi3);

            function EditorGeneric(data) {
                _classCallCheck(this, EditorGeneric);

                console.debug("EditorGeneric: constructor");
                if (!data) {
                    throw new Error("Missing data");
                }
                return _possibleConstructorReturn(this, (EditorGeneric.__proto__ || Object.getPrototypeOf(EditorGeneric)).call(this, data));
            }

            _createClass(EditorGeneric, [{
                key: 'render',
                value: function render() {
                    console.debug("EditorGeneric: render");
                    this.renderUi();
                }
            }, {
                key: 'remove',
                value: function remove() {
                    console.debug("EditorGeneric: remove");
                    this.removeUi();
                    // TODO WTF
                    // if (this.data.getOptions().pl && this.options.placeholderText) { delete this.options.placeholderText; }
                }
            }]);

            return EditorGeneric;
        }(EditorUi);

        return EditorGeneric;
    }
})();

(function () {
    angular.module('switeEditor').factory('EditorTextArea', editorTextAreaFactory);

    function editorTextAreaFactory($compile, deviceDetect, EditorUi) {
        var EditorTextArea = function (_EditorUi4) {
            _inherits(EditorTextArea, _EditorUi4);

            function EditorTextArea(data) {
                _classCallCheck(this, EditorTextArea);

                console.debug("EditorTextArea: constructor");
                if (!data) {
                    throw new Error("Missing data");
                }
                return _possibleConstructorReturn(this, (EditorTextArea.__proto__ || Object.getPrototypeOf(EditorTextArea)).call(this, data));
            }

            _createClass(EditorTextArea, [{
                key: 'selectText',
                value: function selectText(DOMElement) {
                    setTimeout(function () {
                        var doc = document,
                            range,
                            selection;
                        if (doc.body.createTextRange) {
                            range = doc.body.createTextRange();
                            range.moveToElementText(element);
                            range.select();
                        } else if (window.getSelection) {
                            selection = window.getSelection();
                            range = doc.createRange();
                            range.selectNodeContents(DOMElement);
                            selection.removeAllRanges();
                            selection.addRange(range);
                        }
                    });
                }
            }, {
                key: 'selectLastChar',
                value: function selectLastChar(DOMElement) {
                    DOMElement.focus();
                    if (typeof window.getSelection != "undefined" && typeof document.createRange != "undefined") {
                        var range = document.createRange();
                        range.selectNodeContents(DOMElement);
                        range.collapse(false);
                        var sel = window.getSelection();
                        sel.removeAllRanges();
                        sel.addRange(range);
                    } else if (typeof document.body.createTextRange != "undefined") {
                        var textRange = document.body.createTextRange();
                        textRange.moveToElementText(DOMElement);
                        textRange.collapse(false);
                        textRange.select();
                    }
                }

                // Compila il template della TextArea e gli assegna lo scope dello widget che sta venendo editato

            }, {
                key: 'compile',
                value: function compile() {
                    var _this10 = this;

                    console.debug("EditorTextArea: compile");
                    var scope = this.data.getScope();
                    var editableElement = this.data.getElement();
                    var options = this.data.getOptions();

                    options.heightMin = editableElement.height();
                    options.placeholderText = this.data.getPlaceholderText();

                    //When toolbar froala open, means that open virtual Keyboard mobile
                    angular.element('body').addClass('sw-isFroalaActive');

                    scope.TAModel = this.data.isPlaceholder() ? "" : this.data.getEditableData();

                    this.taChangeListener = scope.$watch("TAModel", function (newVal, oldVal) {
                        _this10.emit('TAModel.change', newVal);
                    });

                    this.taOriginalEditable = editableElement.find('[sw-editable]');
                    if (this.taOriginalEditable.attr('editable-options')) {
                        var extendedOptions = this.taOriginalEditable.attr('editable-options');
                        try {
                            this.data.options = options = _.assign(options, JSON.parse(extendedOptions));
                        } catch (e) {
                            console.debug('Error extending textarea options: ' + e.message);
                        }
                    }

                    scope.TAOptions = options;
                    this.taEditableStyle = window.getComputedStyle(this.taOriginalEditable[0]);

                    this.taEditable = this.taOriginalEditable.clone();
                    this.taOriginalEditable.replaceWith(this.taEditable);

                    this.taEditable.attr('froala', 'TAOptions');
                    this.taEditable.attr('ng-model', 'TAModel');
                    var linkFn = $compile(this.taEditable);
                    this.taElement = linkFn(scope);
                }
            }, {
                key: 'render',
                value: function render() {
                    var _this11 = this;

                    this.renderUi();
                    if (!this.data.isCustomizable()) {
                        return;
                    }
                    this.compile();
                    if (!this.taElement || !this.uiElement || !this.taEditable) {
                        throw new Error("Missing data");
                    }

                    this.taToolbar = angular.element('.fr-toolbar');

                    // Prevent a scrollbar to show on widgets textarea in certain cases
                    this.taEditable.find('.fr-wrapper').css('overflow', 'hidden');

                    angular.element('[data-param1="background"]').remove();

                    // Make sure that the text in froala editor has the same style of the original text
                    var frElement = this.taEditable.find('.fr-element');
                    var frPlaceholder = this.taEditable.find('.fr-placeholder');
                    frElement.css('text-align', this.taEditableStyle["text-align"]);
                    frElement.css('min-width', this.data.getElement().width());
                    frElement.css('color', this.taEditableStyle["color"]);
                    frPlaceholder.css('text-align', this.taEditableStyle["text-align"]);
                    frPlaceholder.css('min-width', this.data.getElement().width());

                    frElement.attr('spellcheck', 'false');
                    frElement.attr('autocomplete', 'off');

                    // Remove fucking froala banner
                    var ad = this.taElement.find('[href="https://froala.com/wysiwyg-editor"]');
                    ad.remove();

                    setTimeout(function () {
                        // Focus the element if it's not, to open the soft keyboard
                        frElement.focus();
                        if (!_this11.data.isPlaceholder()) {
                            if (deviceDetect.isMobile() && !deviceDetect.isIpad()) {
                                //TODO:something for mobilegit
                            } else {
                                _this11.selectText(frElement[0]);
                                // Force show froala toolbar
                                _this11.taElement.froalaEditor('toolbar.showInline', null, true);
                            }
                        }
                    });
                }
            }, {
                key: 'remove',
                value: function remove() {
                    console.debug("EditorTextArea: remove");
                    this.removeUi();

                    //When toolbar froala removed, means that virtual Keyboard mobile closed
                    angular.element('body').removeClass('sw-isFroalaActive');

                    if (this.taToolbar) {
                        this.taToolbar.remove();
                    }

                    if (this.taEditable) {
                        this.taElement.replaceWith(this.taOriginalEditable);
                    }

                    if (this.taChangeListener) {
                        this.taChangeListener();
                    }
                    // if (this.data.getOptions().pl && this.options.placeholderText) { delete this.options.placeholderText; }
                }
            }]);

            return EditorTextArea;
        }(EditorUi);

        return EditorTextArea;
    }
})();

(function () {
    angular.module('switeEditor').factory('EditorUi', editorUiFactory);

    function editorUiFactory($compile, $window, Editable) {
        var uiTemplate;

        var EditorUi = function (_EventEmitter) {
            _inherits(EditorUi, _EventEmitter);

            _createClass(EditorUi, null, [{
                key: 'setUiTemplate',
                value: function setUiTemplate(template) {
                    console.debug("EditorUi: setTemplate");
                    if (!template) {
                        throw new Error("Missing UI Template");
                    }
                    uiTemplate = template;
                }
            }]);

            function EditorUi(data) {
                _classCallCheck(this, EditorUi);

                if (!(data instanceof Editable)) {
                    throw new Error("Need Editable instance");
                }

                var _this12 = _possibleConstructorReturn(this, (EditorUi.__proto__ || Object.getPrototypeOf(EditorUi)).call(this));

                _this12.data = data;
                return _this12;
            }

            _createClass(EditorUi, [{
                key: 'getScope',
                value: function getScope() {
                    return this.data.getScope();
                }
            }, {
                key: 'deleteScope',
                value: function deleteScope() {
                    if (this.data) {
                        delete this.data;
                    }
                }
            }, {
                key: 'compileUi',


                // Compila il template della UI e gli assegna lo scope dello widget che sta venendo editato
                value: function compileUi() {
                    console.debug("EditorUi: compile");
                    var template;
                    if (!uiTemplate) {
                        throw new Error("Can't compile UI. Missing data.");
                    }
                    template = uiTemplate.clone();
                    var linkFn = $compile(template);
                    this.uiElement = linkFn(this.data.getScope());
                }
            }, {
                key: 'renderUi',
                value: function renderUi() {
                    console.debug("EditorUi: render");
                    this.compileUi();

                    if (!this.uiElement) {
                        throw new Error("Missing UI Element");
                    }

                    //TODO: trying to prepend to UI to the editable element this.page = angular.element('#page');
                    this.page = angular.element(this.data.getElement()[0]);
                    this.page.prepend(this.uiElement);

                    this.uiToolbarElement = this.uiElement.find('#toolbar');

                    this.setUiPosition();
                }
            }, {
                key: 'setUiPosition',
                value: function setUiPosition() {
                    var editableElement = this.data.getElement();
                    var rect = editableElement[0].getBoundingClientRect();
                    var offsetLeft = this.page[0].getBoundingClientRect().left;
                    var offsetTop = angular.element('.content').scrollTop();

                    this.uiElement.removeAttr('style');
                    this.uiElement.css('right', -10);
                    this.uiElement.css('top', -9);

                    // Each button is 48px, plus 3px border for each side
                    this.uiToolbarElement.css('min-width', 48 * this.data.getButtonsAmount() + 6);

                    this.uiElement.css('z-index', 3);
                    editableElement.css('z-index', 2);
                    editableElement.addClass('edit-mode');
                    editableElement.css('position', editableElement.css('position') === 'static' ? 'relative' : editableElement.css('position'));
                }
            }, {
                key: 'removeUi',
                value: function removeUi() {
                    console.debug("EditorUi: removeUI");

                    var query = '[ui-editable-id="' + this.data.getId() + '"]';
                    var UI = angular.element(query);
                    if (!UI || UI.length === 0) {
                        return;
                    }

                    UI.find("#toolbar").remove();
                    UI.remove();

                    this.data.getElement().removeClass('edit-mode');

                    // blockFactory.removeListener('blockMoved', this.setUiPosition);

                    if (this.uiSidebarSync) {
                        this.uiSidebarSync();
                    }

                    this.data.getElement().removeAttr('style');
                    // this.deleteScope();
                }
            }, {
                key: 'uiCss',
                value: function uiCss(property, value) {
                    return this.uiElement.css(property, value);
                }
            }]);

            return EditorUi;
        }(EventEmitter);

        return EditorUi;
    }
})();

(function () {
    angular.module('switeEditor').factory('switeEditor', switeEditorFactory);

    function switeEditorFactory($templateRequest, _, EventEmitter, EditorUi, EditorTextArea, EditorArray, EditorBrick, EditorGeneric, Editable) {
        var editor, editable;
        var templates = {
            ui: {}
        };
        var editorOptions;
        var _isOpen = false;
        var emitter = new EventEmitter();

        emitter.init = init;
        emitter.isOpen = isOpen;
        emitter.render = render;
        emitter.refresh = refresh;
        emitter.remove = remove;
        emitter.getScope = getScope;
        emitter.uiCss = uiCss;
        emitter.setOptions = setOptions;
        emitter.selectText = selectText;
        emitter.selectLastChar = selectLastChar;
        emitter.loadUiTemplate = loadUiTemplate;
        emitter.getEditables = getEditables;

        return emitter;

        function init(options) {
            var tasks = [];

            if (!templates.text) {
                tasks.push($templateRequest(options.text.templatePath));
            }

            if (!templates.array) {
                tasks.push($templateRequest(options.array.templatePath));
            }

            editorOptions = options || {};

            return Promise.all(tasks).then(function (res) {
                templates.text = angular.element(res[0]);
                templates.array = angular.element(res[1]);
            });
        }

        function isOpen() {
            return _isOpen;
        }

        function setOptions(options) {
            editorOptions = options || {};
        }

        function loadUiTemplate(path, type) {
            return $templateRequest(path).then(function (uiTemplate) {
                templates.ui[type] = uiTemplate;
            });
        }

        function onTaModelChange(newVal) {
            emitter.emit('TAModel.change', newVal);
        }

        function render(data) {
            if (isOpen()) {
                remove();
            }

            EditorUi.setUiTemplate(angular.element(templates.ui[data.uiTemplateType]));
            data = _.omit(data, ['uiTemplateType']);
            var extended = _.assign(data, {
                template: angular.element(templates[data.editable.type]),
                options: editorOptions[data.editable.type]
            });

            editable = new Editable(extended);
            var pluginName = editable.getPlugin();
            var editorClass = eval(pluginName);

            emitter.emit('opened');

            editor = new editorClass(editable);
            editor.on('TAModel.change', onTaModelChange);
            editor.render();
            _isOpen = true;
        }

        function remove() {
            if (!isOpen()) {
                return;
            }

            editor.removeListener('TAModel.change', onTaModelChange);
            editor.remove();
            _isOpen = false;

            emitter.emit('closed');
            emitter.emit('closed.' + editable.getId());
        }

        function getScope() {
            return editable.getScope();
        }

        function getEditables() {
            return editor.getEditables();
        }

        function uiCss(property, value) {
            return editor.uiCss(property, value);
        }

        function refresh(data) {
            if (editor instanceof EditorArray) {
                return editor.refresh(data);
            }

            throw new Error('Using editorArray.refresh method on ' + editor.constructor.name + ' instance');
        }

        function selectText() {
            if (editor instanceof editorTextArea) {
                return editor.selectText();
            }
            throw new Error('Using editorTextArea.selectText method on ' + editor.constructor.name + ' instance');
        }

        function selectLastChar() {
            if (editor instanceof editorTextArea) {
                return editor.selectLastChar();
            }
            throw new Error('Using editorTextArea.selectLastChar method on ' + editor.constructor.name + ' instance');
        }
    }
})();

(function () {
    angular.module('switeEditor').service('deviceDetect', deviceDetect);

    function deviceDetect() {
        return {
            isMobile: function isMobile() {
                var result = navigator.userAgent.match('Android') || navigator.userAgent.match('BlackBerry') || navigator.userAgent.match('iPhone|iPad|iPod') || navigator.userAgent.match('Opera Mini') || navigator.userAgent.match('IEMobile');
                return result !== null;
            },
            isIpad: function isIpad() {
                return navigator.userAgent.match('iPad');
            },
            isIOS: function isIOS() {
                return navigator.userAgent.match('iPhone|iPad|iPod');
            }
        };
    }
})();