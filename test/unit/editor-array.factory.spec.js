describe('test', function(){


//    this.timeout(1); 

    var EditorArray,
        EditorUi,
        $compile,
        $rootScope,
        $timeout,
        editable_conf = {
            template: template,
            options: {
                messages: {
                    removeElement: "Remove Element String",
                    addElement: "Add Element String"
                }
            },
            editable: {
                element: undefined,
                object: {
                    id: '1',
                    data: [{ 
                        title: "La tua posizione", 
                        location: "La tua azienda", 
                        date_from: "Data di inizio", 
                        date_to: "Data di fine", 
                        $$hashKey: "object:1675" 
                    }],
                    isPlaceholder: false,
                    isCustomizable: true
                },
                type: 'array'
            },
            scope: { },
            buttonsAmount: 3,
            arrayTemplate: template,
            uiTemplateType: 'widget',
        },
        template = `"<div id="asd">
                        <ul sw-editable-list-template class="sw-editable-list-template">
                            <li ng-repeat="education in editables" class="education">
                                <div class="tab-li-inside-wrap">
                                    <span class="place">
                                        <span sw-editable-field="date_from">{{ education.date_from.text }}</span>
                                        -
                                        <span sw-editable-field="date_to">{{ education.date_to.text }}</span>
                                    </span>
                                    <span class="tit" sw-editable-field="title" itemprop="title">{{ education.title.text }}</span>
                                    <span>{{ 'presso' | pageTranslate }}</span>
                                    <span sw-editable-field="location" itemprop="jobLocation">{{ education.location.text }}</span>
                                </div>
                            </li>
                        </ul>
                    </div>`;

    beforeEach(module('switeEditor'));
    beforeEach(inject(function (_EditorArray_, _Editable_, _EditorUi_, _$compile_, _$rootScope_, _$timeout_) {
        EditorArray = _EditorArray_;
        Editable = _Editable_;
        EditorUi = _EditorUi_;
        $compile = _$compile_;
        $rootScope = _$rootScope_;
        $timeout = _$timeout_

        EditorUi.setUiPosition = function(){};
    }));


    it('should reject Editable creation for missing data', function(){
        let conf = _.clone(editable_conf);
        delete conf.editable;
        try {
            let editable = new Editable(conf);
        } catch(e){
            expect(e.message === 'Missing data' ).to.be.true;
        }
    });

    it('should reject Editable creation for missing data', function () {
        try {
            let editorArray = new EditorArray(undefined);
        } catch (e) {
            expect(e.message === 'Missing data').to.be.true;
        }
    });


    function getEditorArray( conf ){
        let scope = $rootScope.$new(true);
        // aggiungo il template al dom
        document.body.innerHTML += template;
        let compiledEl = $compile(document.getElementById('asd'))(scope);
        scope.$apply();
        // viene recuperato sa EditorUI e impostato come uiTemplate
        EditorUi.setUiTemplate(compiledEl);
        conf.editable.element = compiledEl;
        conf.scope = scope;
        let editable = new Editable(conf);
        return new EditorArray(editable);
    }



    it('should set (and unset on remove) a global variable for array editing ', function (done) {
        // genero l'editable
        let conf = _.clone(editable_conf);
        let editorArray = getEditorArray( conf );
        
        editorArray.render().should.be.fulfilled
            .then(() => {
                let elId = conf.editable.object.id;
                expect($rootScope.editingEditableArray[elId]).to.be.true;
                editorArray.remove();
                expect($rootScope.editingEditableArray[elId]).to.be.false;

            }).should.notify(done);

        $timeout.flush();
    }); 


    it('should refresh data (removeAll + set new data)', function (done) {
        let conf = _.clone(editable_conf);
        let editorArray = getEditorArray( conf );

        editorArray.render().should.be.fulfilled
            .then(() => {
                let prevDataKeyLength = Object.keys(editorArray.getEditables()).length; // 1
                let newData = [
                    { title: "La tua posizione", location: "La tua azienda", date_from: "Data di inizio", date_to: "Data di fine", $$hashKey: "object:1675"  },
                    { title: "La tua posizione", location: "La tua azienda", date_from: "Data di inizio", date_to: "Data di fine", $$hashKey: "object:1675"  }
                ];
                editorArray.refresh(newData).should.be.fulfilled
                .then(()=>{
                    let thenDataKeyLength = Object.keys(editorArray.getEditables()).length; //2
                    expect(thenDataKeyLength !== prevDataKeyLength).to.be.true;
                });
                
                $timeout.flush();

            }).should.notify(done);

        $timeout.flush();

    });


    it('should add an empty element to editables ', function (done) {
        let conf = _.clone(editable_conf);
        let editorArray = getEditorArray(conf);

        editorArray.render().should.be.fulfilled
            .then(() => {
                let prevDataKeyLength = Object.keys(editorArray.getEditables()).length; // 1
                editorArray.scope.addItem().should.be.fulfilled
                .then(() => {
                    let thenDataKeyLength = Object.keys(editorArray.getEditables()).length; // 1
                    expect(thenDataKeyLength === (prevDataKeyLength + 1));
                }).should.notify(done);

                $timeout.flush();

            }).should.notify(done);

        $timeout.flush();
    }); 


});