// Karma configuration
// Generated on Tue Feb 27 2018 12:06:54 GMT+0100 (CET)

module.exports = function(config) {
  config.set({
    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',
    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['mocha', 'chai-as-promised', 'chai', 'sinon'],
    plugins: [
      'karma-mocha',
      'karma-chai',
      'karma-chai-as-promised',
      'karma-es6-shim',
      'karma-sinon',
      'karma-coverage',
      'karma-chrome-launcher'                                
    ],
    // list of files / patterns to load in the browser
    files: [
      './node_modules/jquery/dist/jquery.min.js',
      './node_modules/angular/angular.js',
      './node_modules/eventemitter3/index.js',
      './node_modules/angular-mocks/angular-mocks.js',
      './node_modules/lodash/lodash.min.js',
      './switeEditor.module.js',
      './lib/**/*.js',
      './test/unit/*.spec.js'
    ],
    // list of files / patterns to exclude
    exclude: [
    ],
    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      // source files, that you wanna generate coverage for 
      // do not include tests or libraries 
      // (these files will be instrumented by Istanbul) 
      './lib/**/*.js': ['coverage']
    },
    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: [
      'progress'
      , 'coverage'
    ],

    // optionally, configure the reporter 
    
    coverageReporter: {
      type: 'html',
      dir: 'test/karmaCodeCoverage'
    },
    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
